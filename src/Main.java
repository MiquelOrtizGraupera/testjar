import ShopDAOS.DAO.comanda.*;
import ShopDAOS.DAO.empleat.*;
import ShopDAOS.DAO.producte.*;
import ShopDAOS.DAO.proveidor.*;

import java.util.List;

public class Main {
    public static void main(String[] args) throws NoSuchMethodError {
        //EX --> 1
//        EmpleatDAO empleatDAO = new EmpleatImpSQL();
//        List<Empleat> empleats = empleatDAO.consultarLlista();
//        empleatDAO = new EmpleatImpFile();
//        empleatDAO.insertarLlista(empleats);
//
//        //EX --> 2
//        EmpleatDAO empleatDAO = new EmpleatImpFile();
//        List<Empleat> llista = empleatDAO.consultarLlista();
//        empleatDAO = new EmpleatImpMongoDB();
//        empleatDAO.insertarLlista(llista);

        //EX --> 3
//        ComandaDAO comandaDAO = new CompandaImplSQL();
//        List<Comanda>listaComanda = comandaDAO.consultarLlista();
//        comandaDAO = new ComandaImplFile();
//        comandaDAO.insertarLlista(listaComanda);

//        //Ex --> 4
//        comandaDAO = new ComandaImplFile();
//        List<Comanda> llistaComanda2 = comandaDAO.consultarLlista();
//        comandaDAO = new CompandaImplMongoDB();
//        comandaDAO.insertarLlista(llistaComanda2);

//        //EX --> 5
//        ProducteDAO producteDAO = new ProducteImplSQL();
//        List<Producte> llistaProductes = producteDAO.consultarLlista();
//        producteDAO = new ProducteImplFile();
//        producteDAO.insertarLlista(llistaProductes);

        //EX --> 6
//      producteDAO = new ProducteImplFile();
//      List<Producte> llistaProductes2 = producteDAO.consultarLlista();
//      producteDAO = new ProducteImplMongoDB();
//      producteDAO.insertarLlista(llistaProductes2);

        //EX --> 7
//        ProveidorDAO proveidorDAO = new ProveidorImplSQL();
//        List<Proveidor> llistaProveidor = proveidorDAO.consultarLlista();
//        proveidorDAO = new ProveidorImplFile();
//        proveidorDAO.insertarLlista(llistaProveidor);

        //EX --> 8
//        proveidorDAO = new ProveidorImplFile();
//        List<Proveidor> llistaProveidor2 = proveidorDAO.consultarLlista();
//        proveidorDAO = new ProveidorImplMongoDB();
//        proveidorDAO.insertarLlista(llistaProveidor2);

        //EX --> 9
//        EmpleatDAO empleatDAO = new EmpleatImpMongoDB();
//        empleatDAO.consultar(7654);

        //EX --> 10

    }
}
